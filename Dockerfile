FROM php:7.4-apache

MAINTAINER "sa5mchavez"

WORKDIR /tmp

RUN apt-get update && apt-get install -y \
    git \
    zip \
    vim \
    curl \
    unzip \
    libicu-dev \
    libbz2-dev \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libreadline-dev \
    libfreetype6-dev \
    g++


RUN docker-php-ext-install mysqli pdo pdo_mysql

## Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
&& rm -rf /usr/local/etc/php-fpm.d/*


WORKDIR /var/www/html
COPY src/ /var/www/html/



RUN chmod 777 -R /var/www/html \
    && chmod 777 -R /var/www/html/storage/logs/ \
    && chown -R www-data:www-data /var/www/html 

COPY 000-default.conf /etc/apache2/sites-available/


# 3. mod_rewrite for URL rewrite and mod_headers for .htaccess extra headers like Access-Control-Allow-Origin-
RUN a2enmod rewrite headers

RUN chmod 755 -R /var/www/html/storage

EXPOSE 80 3306 8000



## docker run -p 80:80 -p 3306 -v $(pwd)/src/:/var/www/html/ Image

